package stash

import (
	"strings"
	"time"

	"github.com/rainycape/memcache"
)

// MC wraps the memcached client so that functionality may be added if needed.
type MC struct {
	*memcache.Client
}

// InitializeMemcached initializes the connections to the memcached hosts that have been passed as parameters.
func InitializeMemcached(hosts string) (*MC, error) {
	var err error

	mc, err := memcache.New(strings.Split(hosts, ",")...)
	if err != nil {
		return nil, err
	}

	mc.SetMaxIdleConnsPerAddr(1000)
	mc.SetTimeout(time.Duration(1) * time.Second)
	return &MC{mc}, err
}
