package stash

import (
	"net/http"
	"net/http/httptest"

	"bitbucket.org/bfitzsimmons/gadgets"
	"github.com/rainycape/memcache"
	"gopkg.in/vmihailenco/msgpack.v2"
)

type cachedResponse struct {
	Headers http.Header
	Code    int
	Data    []byte
}

// Configuration contains the configuration required for the cache middleware.
type Configuration struct {
	AppName    string
	AppVersion string
	Client     *MC
	Timeout    int
}

// CacheMiddleware caches the response from the upstream handler.
func CacheMiddleware(next http.HandlerFunc, config Configuration) http.Handler {
	cacheFunc := func(w http.ResponseWriter, r *http.Request) {
		var (
			err        error
			item       *memcache.Item
			cachedResp = cachedResponse{}
		)

		// Attempt to retrieve the data from the cache.
		cacheKey := gadgets.GenerateCacheKey([]string{config.AppName, "response", r.URL.RequestURI()},
			config.AppVersion)
		item, err = config.Client.Get(cacheKey)
		if err != nil {
			if err != memcache.ErrCacheMiss {
				gadgets.ServerError(w, "Error retrieving reponse object from memcached", err)
				return
			}
		} else {
			// Unpack it.
			if err = msgpack.Unmarshal(item.Value, &cachedResp); err != nil {
				gadgets.ServerError(w, "Error decoding data from memcached", err)
				return
			}

			// Copy the headers from the cached object to the response writer.
			for k, v := range cachedResp.Headers {
				w.Header()[k] = v
			}

			// Set the status.
			w.WriteHeader(cachedResp.Code)

			_, _ = w.Write(cachedResp.Data)
			return
		}

		rec := httptest.NewRecorder()

		// Send the request on to the next handler.
		next.ServeHTTP(rec, r)

		if rec.Code == http.StatusOK || rec.Code == http.StatusMovedPermanently {
			// Populate the cached response struct.
			cachedResp.Code = rec.Code
			cachedResp.Headers = rec.Header()
			cachedResp.Data = rec.Body.Bytes()

			// Encode the data for storage in memcached.
			encData, err := msgpack.Marshal(cachedResp)
			if err != nil {
				gadgets.ServerError(w, "Error encoding data for memcached", err)
				return
			}

			// Write the data to the cache.
			item = &memcache.Item{
				Key:        cacheKey,
				Value:      encData,
				Expiration: int32(config.Timeout),
			}
			if err := config.Client.Set(item); err != nil {
				gadgets.ServerError(w, "Error saving response data in memcached", err)
				return
			}
		}

		// Copy the headers from the request recorder to the response writer.
		for k, v := range rec.Header() {
			w.Header()[k] = v
		}

		// Write the status code.
		w.WriteHeader(rec.Code)

		_, _ = w.Write(rec.Body.Bytes())
	}

	return http.HandlerFunc(cacheFunc)
}
